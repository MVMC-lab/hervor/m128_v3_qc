/**
 * @file stdio.c
 * @author LiYu87
 * @brief 初始化 stdio 相關函式各硬體實作分割。
 * @date 2019.10.13
 */

#if defined(__AVR_ATmega128__)
#    include "m128/stdio.c"
#elif defined(__AVR_ATmega88__) || defined(__AVR_ATmega48__) || \
    defined(__AVR_ATmega168__)
#    include "m88/stdio.c"
#elif defined(__AVR_ATtiny2313__)
#    include "tiny2313/stdio.c"
#else
#    if !defined(__COMPILING_AVR_LIBC__)
#        warning "device type not defined"
#    endif
#endif
