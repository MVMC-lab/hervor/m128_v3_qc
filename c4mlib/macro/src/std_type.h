/**
 * @file std_res.h
 * @author LiYu87
 * @date 2019.03.25
 * @brief 提供標準型態給函式庫使用。
 */

#ifndef C4MLIB_MACRO_STD_TYPE_H
#define C4MLIB_MACRO_STD_TYPE_H

/* Public Section Start */
/**
 * @brief 無回傳、參數為(void *)型態之函式型態。
 */
typedef void (*Func_t)(void *);
/* Public Section End */

#include <stddef.h>
#include <stdint.h>

#endif  // C4MLIB_MACRO_STD_TYPE_H
