#include "c4mlib\bstest\src\bstest.h"

int main() {
    C4M_DEVICE_set();
    DEBUG_INFO("--START--\n");
    SPCR |= (1 << SPE)| (1 << MSTR);
    DDRB = 0b1111;
    DDRD = 0b1010;
    while (1)
    {
        _delay_ms(500);
        DEBUG_INFO("PINF = %d\n", PINF & 1);
        PORTB = 0;
    }
   
    return 0;
}