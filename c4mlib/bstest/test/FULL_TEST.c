#include "c4mlib\bstest\src\bstest.h"

int main() {
    C4M_DEVICE_set();
    int index = 99;
    while (1)
    {
        printf("M128_V3 test system\n");
        printf("Type a number to select test option:\n(1: printf test, 2: boundary scan)\n");
        scanf("%d",&index);
        switch (index)
        {
            case 1:
                printf("C4MLIB_TEST_OK\n");
                break;
            case 2:
                boundary_scan_all();
                break;
            
            default:
                break;
        }
        printf("test compelete!!\n");
    }
    return 0;
}