/**
 * @file bstest.h
 * @author LCY
 * @date 2020.05.20
 * @brief
 *
 * 40pin:
 *          B4 - B5
 *          B6 - B7
 *          D4 - D5
 *          D6 - D7
 *          F0 - F1
 *          F2 - F3
 *          E2 - E3
 *          E4 - E5
 *          E6 - E7
 * 20pin:
 *          G3 - G4
 *          F4 - F5
 *          F6 - F7
 *          B0 - B3
 *          B1 - B2
 *          D2 - D3
 *          D0 - D1
 */

#include "c4mlib/debug/src/debug.h"
#include "c4mlib/device/src/device.h"

// boundary scan dir IO set
// DDRB : whole
#define DDRB_DIR        0b01011000
#define DDRB_MASK       0b11110101
#define DDRB_DIR_INV    (255 - DDRB_DIR)
// DDRD : 0.1.4.5.6.7
#define DDRD_DIR        0b01010001
#define DDRD_MASK       0b11110011
#define DDRD_DIR_INV    (255 - DDRD_DIR)
// DDRE : 2 ~ 7
#define DDRE_DIR        0b01010100
#define DDRE_MASK       0b11111100
#define DDRE_DIR_INV    ((255 - DDRE_DIR) & DDRE_MASK)
// DDRF : whole
#define DDRF_DIR        0b01110101
#define DDRF_MASK       0b01110000
#define DDRF_DIR_INV    (255 - DDRF_DIR)
// DDRG : 3.4
#define DDRG_DIR        0b01000
#define DDRG_MASK       0b11000
#define DDRG_DIR_INV    ((255 - DDRG_DIR) & DDRG_MASK)

void boundary_scan_dir_1(void);
void boundary_scan_dir_2(void);
void boundary_scan_E_dir_1(void);
void boundary_scan_E_dir_2(void);
void boundary_scan_F_dir_1(void);
void boundary_scan_F_dir_2(void);
void boundary_scan_G_dir_1(void);
void boundary_scan_G_dir_2(void);
void boundary_scan_D_dir_1(void);
void boundary_scan_D_dir_2(void);
void boundary_scan_B_dir_1(void);
void boundary_scan_B_dir_2(void);

void boundary_scan_all(void);

void DDRB_B1_B2(void);
void DDRB_B0_B3(void);
void DDRD_D2_D3(void);