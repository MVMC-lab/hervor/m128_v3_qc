/**
 * @file bstest.c
 * @author LCY
 * @date 2020.05.20
 * @brief
 *
 */

#include "c4mlib/bstest/src/bstest.h"

#define SCAN_OK 2

void boundary_scan_all(void){
    boundary_scan_B_dir_1();
    boundary_scan_B_dir_2();
    boundary_scan_D_dir_1();
    boundary_scan_D_dir_2();
    boundary_scan_E_dir_1();
    boundary_scan_E_dir_2();
    boundary_scan_F_dir_1();
    boundary_scan_F_dir_2();
    boundary_scan_G_dir_1();
    boundary_scan_G_dir_2();
    DDRB_B0_B3();
    DDRB_B1_B2();
    DDRD_D2_D3();
}

void boundary_scan_B_dir_1(void) {
    DDRB = 0b01010000;

    PORTB = 0b01010000;
    for (int i = 0; i < 2; i++)
        __asm__("nop");

    if ((PINB & 0b01010000) == 0b01010000) {
        printf("PORT[B]_direction_1_v1---OK\n");
    }
    else {
        printf("PORT[B]_direction_1_v1---XXX\n");
    }
    PORTB &= ~(0b01010000);
    for (int i = 0; i < 2; i++)
        __asm__("nop");
    if ((PINB & 0b01010000) == 0) {
        printf("PORT[B]_direction_1_v2---OK\n");
    }
    else {
        printf("PORT[B]_direction_1_v2---XXX\n");
    }
}

void boundary_scan_B_dir_2(void) {
    DDRB = 0b10100000;

    PORTB = 0b10100000;
    for (int i = 0; i < 2; i++)
        __asm__("nop");

    if ((PINB & 0b10100000) == 0b10100000) {
        printf("PORT[B]_direction_2_v1---OK\n");
    }
    else {
        printf("PORT[B]_direction_2_v1---XXX\n");
    }
    PORTB &= ~(0b10100000);
    for (int i = 0; i < 2; i++)
        __asm__("nop");
    if ((PINB & 0b10100000) == 0) {
        printf("PORT[B]_direction_2_v2---OK\n");
    }
    else {
        printf("PORT[B]_direction_2_v2---XXX\n");
    }
}

void boundary_scan_D_dir_1(void) {
    DDRD = 0b01010001;

    PORTD = 0b01010001;
    for (int i = 0; i < 2; i++)
        __asm__("nop");

    if ((PIND & 0b01010001) == 0b01010001) {
        printf("PORT[D]_direction_1_v1---OK\n");
    }
    else {
        printf("PORT[D]_direction_1_v1---XXX\n");
    }
    PORTD &= ~(0b01010001);
    for (int i = 0; i < 2; i++)
        __asm__("nop");
    if ((PIND & 0b01010001) == 0) {
        printf("PORT[D]_direction_1_v2---OK\n");
    }
    else {
        printf("PORT[D]_direction_1_v2---XXX\n");
    }
}

void boundary_scan_D_dir_2(void) {
    DDRD = 0b10100010;

    PORTD = 0b10100010;
    for (int i = 0; i < 2; i++)
        __asm__("nop");

    if ((PIND & 0b10100010) == 0b10100010) {
        printf("PORT[D]_direction_2_v1---OK\n");
    }
    else {
        printf("PORT[D]_direction_2_v1---XXX\n");
    }
    PORTD &= ~(0b10100010);
    for (int i = 0; i < 2; i++)
        __asm__("nop");
    if ((PIND & 0b10100010) == 0) {
        printf("PORT[D]_direction_2_v2---OK\n");
    }
    else {
        printf("PORT[D]_direction_2_v2---XXX\n");
    }
}


void boundary_scan_E_dir_1(void) {
    DDRE = 0b10101000;

    PORTE = 0b10101000;
    for (int i = 0; i < 2; i++)
        __asm__("nop");

    if ((PINE & 0b01010100) == 0b01010100) {
        printf("PORT[E]_direction_1_v1---OK\n");
    }else{
        printf("PORT[E]_direction_1_v1---XXX\n");
    }
    

    PORTE &= ~(0b10101000);
    for (int i = 0; i < 2; i++)
        __asm__("nop");
    if ((PINE & 0b01010100) == 0) {
        printf("PORT[E]_direction_1_v2---OK\n");
    }else{
        printf("PORT[E]_direction_1_v2---XXX\n");
    }
}

void boundary_scan_E_dir_2(void){
    DDRE = 0b01010100;

    PORTE = 0b01010100;
    for (int i = 0; i < 2; i++)
        __asm__("nop");

    if ((PINE & 0b01010100) == 0b01010100) {
        printf("PORT[E]_direction_2_v1---OK\n");
    }
    else {
        printf("PORT[E]_direction_2_v1---XXX\n");
    }

    PORTE &= ~(0b01010100);
    for (int i = 0; i < 2; i++)
        __asm__("nop");
    if ((PINE & 0b01010100) == 0) {
        printf("PORT[E]_direction_2_v2---OK\n");
    }
    else {
        printf("PORT[E]_direction_2_v2---XXX\n");
    }
}

void boundary_scan_F_dir_1(void){
    DDRF = 0b01010101;

    PORTF = 0b01010101;
    for (int i = 0; i < 2; i++)
        __asm__("nop");

    if ((PINF & 0b01010101) == 0b01010101) {
        printf("PORT[F]_direction_1_v1---OK\n");
    }
    else {
        printf("PORT[F]_direction_1_v1---XXX\n");
    }

    PORTF &= ~(0b01010101);
    for (int i = 0; i < 2; i++)
        __asm__("nop");
    if ((PINF & 0b01010101) == 0) {
        printf("PORT[F]_direction_1_v2---OK\n");
    }
    else {
        printf("PORT[F]_direction_1_v2---XXX\n");
    }
}

void boundary_scan_F_dir_2(void){
    DDRF = 0b10101010;

    PORTF = 0b10101010;
    for (int i = 0; i < 2; i++)
        __asm__("nop");

    if ((PINF & 0b10101010) == 0b10101010) {
        printf("PORT[F]_direction_2_v1---OK\n");
    }
    else {
        printf("PORT[F]_direction_2_v1---XXX\n");
    }

    PORTF &= ~(0b10101010);
    for (int i = 0; i < 2; i++)
        __asm__("nop");
    if ((PINF & 0b10101010) == 0) {
        printf("PORT[F]_direction_2_v2---OK\n");
    }
    else {
        printf("PORT[F]_direction_2_v2---XXX\n");
    }
}

void boundary_scan_G_dir_1(void){
    DDRG = 0b01000;

    PORTG = 0b01000;
    for (int i = 0; i < 2; i++)
        __asm__("nop");

    if ((PING & 0b01000) == 0b01000) {
        printf("PORT[G]_direction_1_v1---OK\n");
    }
    else {
        printf("PORT[G]_direction_1_v1---XXX\n");
    }
    PORTG &= ~(0b01000);
    for (int i = 0; i < 2; i++)
        __asm__("nop");
    if ((PING & 0b01000) == 0) {
        printf("PORT[G]_direction_1_v2---OK\n");
    }
    else {
        printf("PORT[G]_direction_1_v2---XXX\n");
    }
}

void boundary_scan_G_dir_2(void){
    DDRG = 0b10000;
    PORTG = 0b10000;
    for (int i = 0; i < 2; i++)
        __asm__("nop");

    if ((PING & 0b10000) == 0b10000) {
        printf("PORT[G]_direction_2_v1---OK\n");
    }
    else {
        printf("PORT[G]_direction_2_v1---XXX\n");
    }

    PORTG &= ~(0b10000);
    for (int i = 0; i < 2; i++)
        __asm__("nop");
    if ((PING & 0b10000) == 0) {
        printf("PORT[G]_direction_2_v2---OK\n");
    }
    else {
        printf("PORT[G]_direction_2_v2---XXX\n");
    }
}

void DDRB_B1_B2(void){
    DDRB = 0b100;
    uint8_t count = 0;
    uint8_t wrong = 0;
    while (count < 10)
    {
        PORTB ^= 0b100;
        for (long i = 0; i < 2; i++)
            __asm__("nop");
        if ((PINB & 0b10) != ((PORTB & 0b100) >> 1)) {
          wrong++;
        }
        for (long i = 0; i < 2; i++)
            __asm__("nop");
        count++;
        // printf("C = %d\n", count);
    }

    DDRB = 0b10;
    count = 0;
    while (count < 10)
    {
        PORTB ^= 0b10;
        for (long i = 0; i < 2; i++)
            __asm__("nop");
        if ((PINB & 0b100) != ((PORTB & 0b10) << 1)) {
          wrong++;
        }
        for (long i = 0; i < 2; i++)
            __asm__("nop");
        count++;
        // printf("C = %d\n", count);
    }
    if (wrong == 0) {
        printf("B1 & B2---OK\n");
    }else
    {
        printf("B1 & B2---XXX\n");
    }
}

void DDRB_B0_B3(void){
    DDRB = 0b1;
    uint8_t count = 0;
    uint8_t wrong = 0;
    while (count < 10) {
        PORTB ^= 0b1;
        for (long i = 0; i < 2; i++)
            __asm__("nop");
        if ((PINB & 0b1000) != ((PORTB & 0b1) << 3)) {
            wrong++;
        }
        for (long i = 0; i < 2; i++)
            __asm__("nop");
        count++;
        // printf("PINB = %d\n", (PINB & 0b1000));
    }

    DDRB = 0b1000;
    count = 0;
    while (count < 10) {
        PORTB ^= 0b1000;
        for (int i = 0; i < 2; i++)
        __asm__("nop");
        if ((PINB & 0b1) != ((PORTB & 0b1000) >> 3)) {
        wrong++;
        }
        for (int i = 0; i < 2; i++)
            __asm__("nop");
        count++;
        // printf("PINB = %d\n", (PINB & 0b1));
    }
    if (wrong == 0) {
        printf("B0 & B3---OK\n");
    }else
    {
        printf("B0 & B3---XXX\n");
    }
}

void DDRD_D2_D3(void){
    DDRD = 0b1000;
    uint8_t count = 0;
    uint8_t wrong = 0;
    while (count < 10) {
        PORTD ^= 0b1000;
        for (long i = 0; i < 2; i++)
        __asm__("nop");
        if ((PIND & 0b100) != ((PORTD & 0b1000) >> 1)) {
            wrong++;
        }
        for (long i = 0; i < 2; i++)
        __asm__("nop");
        count++;
        // printf("PIND = %d\n", (PIND & 0b100));
    }

    DDRD = 0b100;
    count = 0;
    while (count < 10) {
        PORTD ^= 0b100;
        for (int i = 0; i < 2; i++)
        __asm__("nop");
        if ((PIND & 0b1000) != ((PORTD & 0b100) << 1)) {
            wrong++;
        }
        for (int i = 0; i < 2; i++)
        __asm__("nop");
        count++;
        // printf("PIND = %d\n", (PIND & 0b1000));
    }
    if (wrong == 0) {
        printf("D2 & D3---OK\n");
    } else {
        printf("D2 & D3---XXX\n");
    }
}